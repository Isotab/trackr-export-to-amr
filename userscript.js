// ==UserScript==
// @name         Trackr to AMR Export
// @namespace    jlsduglKUGFIULSDFd6465*(&r7ysbk)
// @version      0.0.2
// @description  Export manga from trackr.moe into a AMR compatible format
// @author       Isotab
// @match        https://trackr.moe/user/dashboard
// @downloadURL  https://gitlab.com/Isotab/trackr-export-to-amr/raw/master/userscript.js
// @updateURL    https://gitlab.com/Isotab/trackr-export-to-amr/raw/master/userscript.js
// @grant        none
// ==/UserScript==


(function($) {
    
    function getHostname(url) {
        let element = document.createElement('a')
        element.href = url

        return element.hostname
    }

    function getMirror(mangaUrl) {
        let host = getHostname(mangaUrl)
        
        if (host.includes('mangadex.org')) {
            return 'MangaDex'
        }
        
        return null // Default so we skip this if it has not been added or is not supported by AMR
    }

    function buildList() {
        let results = {
            converted: [],
            skipped: []
        }

        $('table.tablesorter-bootstrap tbody tr').each(function(index) {
            let cells = $(this).find('td')
            let titleCell = $(cells[1])
            let currentCell = $(cells[2])
    
            let titleLink = titleCell.find('a.title:first').attr('href')
            let titleName = titleCell.find('a.title:first').text()
            let currentChapter = currentCell.find('a').attr('href')

            let mirror = getMirror(titleLink)
            
            if (mirror !== null) {
                results.converted.push({
                    "m": "MangaDex",
                    "n": titleName,
                    "u": titleLink,
                    "l": currentChapter
                })   
            } else {
                results.skipped.push({
                    "name": titleName,
                    "link": titleLink
                })
            }
        })

        return results
    }

    $('#navbarNavDropdown ul').prepend('<li class="nav-item"><button class="btn btn-dark btn-sm" id="exportToAmrButton">Export to AMR</a></li>')
    $('body').append(`
        <div class="modal fade" tabindex="-1" id="exportToAmrModal">
            <div class="modal-dialog modal-dialog-scrollable mw-100 w-75">
                <div class="modal-content bg-dark">
                    <div class="modal-header">
                        <h5 class="modal-title">Export to AMR</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-auto">
                                    <input type="text" class="form-control" id="exportToAmrList">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-primary" id="exportToAmrCopy">Copy</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col"><h5>Skipped Manga</h5></div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table id="exportToAmrTable" class="table table-dark">
                                        <thead>
                                            <tr>
                                                <th>Manga Name</th>
                                                <th>Url</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `)

    $('#exportToAmrButton').click(() => {
        let list = buildList()
        $('#exportToAmrList').val(JSON.stringify({
            "mangas": list.converted
        }))
        
        $('#exportToAmrTable tbody').empty()
        list.skipped.forEach(value => {
            $('#exportToAmrTable').append(`
                <tr>
                    <td>${value.name}</td>
                    <td>${value.url}</td>
                </tr>
            `)
        })
        $('#exportToAmrModal').modal('show')
    })

    $('#exportToAmrCopy').click(() => {
        let copyTarget = document.getElementById('exportToAmrList')
        copyTarget.select()
        document.execCommand('copy')
    })

})(jQuery)